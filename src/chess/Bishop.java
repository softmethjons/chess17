/**
 * 
 */
package chess;

/**
 * Bishop extends Chesspiece. It is able to move diagonally,
 * and can continue moving in that direction if it is not
 * blocked or does not go out of bounds.
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 */
public class Bishop extends Chesspiece {

	public Bishop(char side, int col, int row, chesstype chesspiece,
			Chesspiece[][] chessboard) {
		super(side, col, row, chesspiece, chessboard);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Updates the Bishop's moveset. It scans for enemies and 
	 * empty spaces. Bishops can move diagonally, if it is valid,
	 * and kill (1).
	 * @param None
	 * @return None  
	 */
	@Override
	public void updateMoves() {
		resetBoard();
		
		/*
		 * Up and left
		 */
		for (int i = row - 1, j = col - 1; i > -1 && j > -1; i--, j--)	{
			if (chessboard[i][j] != null) {
				if (chessboard[i][j].side != this.side) {
					moveSet[i][j] = 1;
				}
				break;
			}
			moveSet[i][j] = 1;
		}
		
		/*
		 * up and right
		 */
		for (int i = row - 1, j = col + 1; i > -1 && j < 8; i--, j++) {
			if (chessboard[i][j] != null) {
				if (chessboard[i][j].side != this.side) {
					moveSet[i][j] = 1;
				}
				break;
			}
			moveSet[i][j] = 1;
		}
		
		/*
		 * down and right
		 */
		for (int i = row + 1, j = col + 1; i < 8 && j < 8; i++, j++) {
			if (chessboard[i][j] != null) {
				if (chessboard[i][j].side != this.side) {
					moveSet[i][j] = 1;
				}
				break;
			}
			moveSet[i][j] = 1;
		}
		
		/*
		 * down and left
		 */
		for (int i = row + 1, j = col - 1; i < 8 && j > -1; i++, j--) {
			if (chessboard[i][j] != null) {
				if (chessboard[i][j].side != this.side) {
					moveSet[i][j] = 1;
				}
				break;
			}
			moveSet[i][j] = 1;
		}
	}
	
}
