/**
 * 
 */
package chess;

import java.util.StringTokenizer;

/**
 * A player of the game. They make the moves that
 * determine the placement of their pieces.
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 */
public class Player {

	private Chessboard gameboard;
	private Chesspiece[][] chessboard;
	char side; 

	public Player (char side, Chessboard chessboard) {
		this.gameboard = chessboard;
		this.chessboard = chessboard.chessboard;
		if (Character.toLowerCase(side) != 'w' && Character.toLowerCase(side) != 'b')
			throw new IllegalArgumentException("Invalid Side!");
		this.side = side;
	}

	/**
	 * Reads in the player's move.
	 * @param play These are the coordinates that the player inputs
	 * @return boolean This determines whether the player's move is valid or not
	 */
	public boolean move(String play) {
		String currPos, newPos;
		String promo = "";
		int colCurr, rowCurr, colNew, rowNew;

		StringTokenizer st = new StringTokenizer(play);
		if (!st.hasMoreTokens())
			return false;
		currPos = st.nextToken();
		if (currPos.length() != 2)
			return false;
		if (!st.hasMoreTokens())
			return false;
		newPos = st.nextToken();
		if (newPos.length() != 2)
			return false;
		if (st.hasMoreTokens()) {
			promo = st.nextToken();
			if (!promo.equalsIgnoreCase("B") && !promo.equalsIgnoreCase("N") && !promo.equalsIgnoreCase("R") && !promo.equalsIgnoreCase("Q") && !promo.equalsIgnoreCase("draw?"))
				return false;
			if (st.hasMoreTokens())
				return false;
		}

		if (currPos.equalsIgnoreCase(newPos))
			return false;

		//Extraction of int positions
		if ((colCurr = getNum(currPos.charAt(0)+ "")) == -1)
			return false;
		try {
			rowCurr = 8 - Integer.parseInt(currPos.charAt(1)+"");
		} catch (NumberFormatException | StringIndexOutOfBoundsException t) {
			return false;
		}

		if ((colNew = getNum(newPos.charAt(0)+ "")) == -1)
			return false;
		try {
			rowNew = 8 - Integer.parseInt(newPos.charAt(1)+"");
		} catch (NumberFormatException | StringIndexOutOfBoundsException t) {
			return false;
		}

		Chesspiece temp = chessboard[rowCurr][colCurr];
		if (temp == null)
			return false;

		if (!temp.isMyPiece(this))
			return false;

		if (temp.piece == chesstype.pawn){
			return (playPawn((Pawn)temp,colNew,rowNew, promo));	
		}
		else if (temp.piece == chesstype.rook || temp.piece == chesstype.bishop || temp.piece == chesstype.knight || temp.piece == chesstype.queen || temp.piece == chesstype.king){
			return (playPiece(temp,colNew,rowNew, promo));	
		}
		return false;
	}

	/**
	 * Plays a pawn.
	 * @param temp This is the pawn that will be played (if the move is valid)
	 * @param colNew This is the column that the pawn is moving to
	 * @param rowNew This is the row that the pawn is moving to
	 * @param mess This is the message that the user appended (if there is one)
	 * @return boolean This determines if the pawn can make its move
	 */
	private boolean playPawn(Pawn temp, int colNew, int rowNew, String mess) {
		//Checks for obstructions
		if (temp.isValidMove(colNew, rowNew) || temp.isKillable(colNew, rowNew)) {
			if (temp.piece == chesstype.pawn && chessboard[temp.getRow()][temp.getCol()].moveSet[rowNew][colNew] == 3) {
				return gameboard.enPassant(temp.getCol(), temp.getRow(), colNew, rowNew);
			}
			if (gameboard.move(temp.getCol(), temp.getRow(), colNew, rowNew, mess)) {
				return true;
			}
			else
				return false;
		}
		return false;
	}

	/**
	 * Plays a piece that is not a pawn.
	 * @param temp This is the piece that will be played (if the move is valid)
	 * @param colNew This is the column that the piece is moving to
	 * @param rowNew This is the row that the piece is moving to
	 * @param mess This is the message that the user appended (if there is one)
	 * @return boolean This determines if the piece can make its move
	 */
	private boolean playPiece(Chesspiece temp, int colNew, int rowNew, String mess) {
		if (temp.isValidMove(colNew, rowNew)) {
			if (temp.piece == chesstype.king && chessboard[temp.getRow()][temp.getCol()].moveSet[rowNew][colNew] == 2) {
				gameboard.castle(temp.getCol(), temp.getRow(), colNew, rowNew);
				return true;
			}
			if (gameboard.move(temp.getCol(), temp.getRow(), colNew, rowNew, mess))
				return true;
			else
				return false;
		}
		return false;
	}

	/**
	 * Determines the numerical representation of
	 * a letter that the player inputs.
	 * @param x This is the letter that the player input
	 * @return int This is the numerical representation of the input letter
	 */
	private int getNum(String x) {
		return "abcdefgh".indexOf(x);
	}
}