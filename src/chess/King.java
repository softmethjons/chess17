/**
 * 
 */
package chess;

/**
 * King extends Chesspiece. It is only able to move if it
 * will not be putting itself in check or checkmate.
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 * 
 */
public class King extends Chesspiece{
	private Chessboard cb;

	public King(char side, int col, int row, chesstype chesspiece, Chesspiece[][] chessboard, Chessboard cb) {
		super(side, col, row, chesspiece, chessboard);
		this.cb = cb;
	}
	
	/**
	 * Updates the King's moveset. It scans for enemies and 
	 * empty spaces. Kings can move in any direction, if it
	 * is valid, and kill (1).
	 * @param None
	 * @return None  
	 */
	@Override
	public void updateMoves() {
		resetBoard();

		/*
		 * Up and left
		 */
		if (row > 0 && col > 0) {
			if (chessboard[row-1][col-1] == null) {
				if (!scanForKill(this.side, this.getCol()-1, this.getRow()-1))
					moveSet[row-1][col-1] = 1;
			}
			else {
				if (chessboard[row-1][col-1].side != this.side && !scanForKill(this.side, this.getCol()-1, this.getRow()-1))
					moveSet[row-1][col-1] = 1;
			}
		}
		
		/*
		 * Up and right
		 */
		if (row > 0 && col < 7) {
			if (chessboard[row-1][col+1] == null) {
				if (!scanForKill(this.side, this.getCol()+1, this.getRow()-1))
					moveSet[row-1][col+1] = 1;
			}
			else {
				if (chessboard[row-1][col+1].side != this.side && !scanForKill(this.side, this.getCol()+1, this.getRow()-1))
					moveSet[row-1][col+1] = 1;
			}
		}
		
		/*
		 * Up
		 */
		if (row > 0) {
			if (chessboard[row-1][col] == null) {
				if (!scanForKill(this.side, this.getCol(), this.getRow()-1))
					moveSet[row-1][col] = 1;
			}
			else {
				if (chessboard[row-1][col].side != this.side && !scanForKill(this.side, this.getCol(), this.getRow()-1))
					moveSet[row-1][col] = 1;
			}
		}

		/*
		 * Down and left
		 */
		if (row < 7 && col > 0) {
			if (chessboard[row+1][col-1] == null) {
				if (!scanForKill(this.side, this.getCol()-1, this.getRow()+1))
					moveSet[row+1][col-1] = 1;
			}
			else {
				if (chessboard[row+1][col-1].side != this.side && !scanForKill(this.side, this.getCol()-1, this.getRow()+1))
					moveSet[row+1][col-1] = 1;
			}
		}
		
		/*
		 * Down and right
		 */
		if (row < 7 && col < 7) {
			if (chessboard[row+1][col+1] == null) {
				if (!scanForKill(this.side, this.getCol()+1, this.getRow()+1))
					moveSet[row+1][col+1] = 1;
			}
			else {
				if (chessboard[row+1][col+1].side != this.side && !scanForKill(this.side, this.getCol()+1, this.getRow()+1))
					moveSet[row+1][col+1] = 1;
			}
		}
		
		/*
		 * Down
		 */
		if (row < 7) {
			if (chessboard[row+1][col] == null) {
				if (!scanForKill(this.side, this.getCol(), this.getRow()+1))
					moveSet[row+1][col] = 1;
			}
			else {
				if (chessboard[row+1][col].side != this.side && !scanForKill(this.side, this.getCol(), this.getRow()+1))
					moveSet[row+1][col] = 1;
			}
		}
		
		/*
		 * Left
		 */
		if (col > 0) {
			if (chessboard[row][col-1] == null) {
				if (!scanForKill(this.side, this.getCol()-1, this.getRow()))
					moveSet[row][col-1] = 1;
			}
			else {
				if (chessboard[row][col-1].side != this.side && !scanForKill(this.side, this.getCol()-1, this.getRow()))
					moveSet[row][col-1] = 1;
			}
		}
		
		/*
		 * Right
		 */
		if (col < 7) {
			if (chessboard[row][col+1] == null) {
				if (!scanForKill(this.side, this.getCol()+1, this.getRow()))
					moveSet[row][col+1] = 1;
			}
			else {
				if (chessboard[row][col+1].side != this.side && !scanForKill(this.side, this.getCol()+1, this.getRow()))
					moveSet[row][col+1] = 1;
			}
		}
		
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (moveSet[i][j] == 1) {
					if (cb.falsePositive(this.side, j,i, this.col, this.row))
						moveSet[i][j] = 0;
				}
			}
		}
		
		/*
		 * Castling
		 */
		if (firstMove == 0 && !scanForKill(side, col, row)) {
			int leftCount = 0;
			int rightCount = 0;
			for (int i = col-1; i > 0; i--) {
				if (chessboard[row][i] == null) { 
					if (i > 1) {
						if(scanForKill(side, i, row))
							break;
					}
					leftCount++;
				}
				else
					break;
			}
			for (int i = col+1; i < 7; i++) {
				if (chessboard[row][i] == null && !scanForKill(side, i, row))
					rightCount++;
				else
					break;
			}
			if (leftCount == 3) {
				if (chessboard[row][0] != null && chessboard[row][0].firstMove == 0)
					moveSet[row][col-2] = 2;
			}
			if (rightCount == 2) {
				if (chessboard[row][7] != null && chessboard[row][7].firstMove == 0)
					moveSet[row][col+2] = 2;
			}
		}
	}
	
	/**
	 * Checks to see if the King is in check.  
	 * @param None
	 * @return boolean This determines whether the King is in check or not
	 */
	public boolean isCheck() {
		return (scanForKill(this.side, this.col, this.row));
	}
	
	/**
	 * Checks to see if the King is in checkmate.
	 * @param None
	 * @return boolean This determines whether the King is in checkmate or not
	 */
	public boolean isCheckmate() {
		/*
		 * Checks if the King can make any safe moves.
		 */
		if (firstMove == 0 && moveSet[row][col-2] == 2)
			return false;
		
		if (firstMove == 0 && moveSet[row][col+2] == 2)
			return false;
		
		if (row > 0 && col > 0) {
			if (moveSet[row-1][col-1] == 1)
				return false;
		}

		if (row > 0 && col < 7) {
			if (moveSet[row-1][col+1] == 1)
				return false;
		}

		if (row > 0) {
			if (moveSet[row-1][col] == 1)
				return false;
		}

		if (row < 7 && col > 0) {
			if (moveSet[row+1][col-1] == 1)
				return false;
		}

		if (row < 7 && col < 7) {
			if (moveSet[row+1][col+1] == 1)
				return false;
		}

		if (row < 7) {
			if (moveSet[row+1][col] == 1)
				return false;
		}

		if (col > 0) {
			if (moveSet[row][col-1] == 1)
				return false;
		}

		if (col < 7) {
			if (moveSet[row][col+1] == 1)
				return false;
		}
		
		/*
		 * Checks if the piece putting the King in check can be killed.
		 */
		Chesspiece threat = null;
		for (Chesspiece[] tempArr : chessboard) {
			for (Chesspiece temp: tempArr) {
				if (temp != null) {
					if (temp.side != side) {
						if (temp.moveSet[row][col] == 1) {
							if (temp.scanForKill(temp.side, temp.getCol(), temp.getRow()))
								return false;
							else {
								threat = temp;
								/*
								 * Checks if there is a friendly piece that can sacrifice itself to save the King.
								 */
								if (canBlock(threat, col, row))
									return false;
							}
						}
					}
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Performs a search to see if any piece can block a threat
	 * and sacrifice itself to protect the King.
	 * @param threat The threat to the King
	 * @param col The column that the King is located in
	 * @param row The row that the King is located in
	 * @return boolean This determines whether a piece can block the threat or not
	 */
	public boolean canBlock(Chesspiece threat, int col, int row) {
		int tempRow = -1;
		int tempCol = -1;
		Chesspiece king = chessboard[row][col];
		if (threat.piece == chesstype.rook || threat.piece == chesstype.queen) {
			/*
			 * Horizontal cases
			 */
			for (int i = threat.getCol()+1; i < 8; i++) {
				if (chessboard[threat.getRow()][i] != null && chessboard[threat.getRow()][i] == king) {
					if (threat.getCol() != i-1)
						tempCol = i-1;
					break;
				}
			}
			if (tempCol != -1) {
				for (int i = tempCol; i > threat.getCol(); i--) {
					for (Chesspiece[] tempArr : chessboard) {
						for (Chesspiece temp: tempArr) {
							if (temp != null) {
								if (temp.side == king.side) {
									if (temp.moveSet[threat.getRow()][i] == 1 || (temp.piece == chesstype.pawn && temp.moveSet[threat.getRow()][i] == 2) || (temp.piece == chesstype.pawn && temp.moveSet[threat.getRow()][i] == 3))
										return true;
								}
							}
						}
					}
				}
				tempCol = -1;
			}
			for (int i = threat.getCol()-1; i > -1; i--) {
				if (chessboard[threat.getRow()][i] != null && chessboard[threat.getRow()][i] == king) {
					if (threat.getCol() != i+1)
						tempCol = i+1;
					break;
				}
			}
			if (tempCol != -1) {
				for (int i = tempCol; i < threat.getCol(); i++) {
					for (Chesspiece[] tempArr : chessboard) {
						for (Chesspiece temp: tempArr) {
							if (temp != null) {
								if (temp.side == king.side) {
									if (temp.moveSet[threat.getRow()][i] == 1 || (temp.piece == chesstype.pawn && temp.moveSet[threat.getRow()][i] == 2) || (temp.piece == chesstype.pawn && temp.moveSet[threat.getRow()][i] == 3))
										return true;
								}
							}
						}
					}
				}
				tempCol = -1;
			}
			
			/*
			 * Vertical cases
			 */
			for (int i = threat.getRow()+1; i < 8; i++) {
				if (chessboard[i][threat.getCol()] != null && chessboard[i][threat.getCol()] == king) {
					if (threat.getRow() != i-1)
						tempRow = i-1;
					break;
				}
			}
			if (tempRow != -1) {
				for (int i = tempRow; i > threat.getRow(); i--) {
					for (Chesspiece[] tempArr : chessboard) {
						for (Chesspiece temp: tempArr) {
							if (temp != null) {
								if (temp.side == king.side) {
									if (temp.moveSet[i][threat.getCol()] == 1 || (temp.piece == chesstype.pawn && temp.moveSet[i][threat.getCol()] == 2) || (temp.piece == chesstype.pawn && temp.moveSet[i][threat.getCol()] == 3))
										return true;
								}
							}
						}
					}
				}
				tempRow = -1;
			}
			for (int i = threat.getRow()-1; i > -1; i--) {
				if (chessboard[i][threat.getCol()] != null && chessboard[i][threat.getCol()] == king) {
					if (threat.getRow() != i+1)
						tempRow = i+1;
					break;
				}
			}
			if (tempRow != -1) {
				for (int i = tempRow; i < threat.getRow(); i++) {
					for (Chesspiece[] tempArr : chessboard) {
						for (Chesspiece temp: tempArr) {
							if (temp != null) {
								if (temp.side == king.side) {
									if (temp.moveSet[i][threat.getCol()] == 1 || (temp.piece == chesstype.pawn && temp.moveSet[i][threat.getCol()] == 2) || (temp.piece == chesstype.pawn && temp.moveSet[i][threat.getCol()] == 3))
										return true;
								}
							}
						}
					}
				}
				tempRow = -1;
			}
		}
		if (threat.piece == chesstype.bishop || threat.piece == chesstype.queen) {
			/*
			 * Up and left cases
			 */
			for (int i = threat.getRow() - 1, j = threat.getCol() - 1; i > -1 && j > -1; i--, j--)	{
				if (chessboard[i][j] != null && chessboard[i][j] == king) {
					if (threat.getRow() != i+1 && threat.getCol() != j+1) {
						tempRow = i+1;
						tempCol = j+1;
					}
					break;
				}
			}
			if (tempRow != -1 && tempCol != -1) {
				for (int i = tempRow, j = tempCol; i < threat.getRow() && j < threat.getCol(); i++, j++) {
					for (Chesspiece[] tempArr : chessboard) {
						for (Chesspiece temp: tempArr) {
							if (temp != null) {
								if (temp.side == king.side) {
									if (temp.moveSet[i][j] == 1 || (temp.piece == chesstype.pawn && temp.moveSet[i][j] == 2) || (temp.piece == chesstype.pawn && temp.moveSet[i][j] == 3))
										return true;
								}
							}
						}
					}
				}
				tempRow = -1;
				tempCol = -1;
			}
			
			/*
			 * Up and right cases
			 */
			for (int i = threat.getRow() - 1, j = threat.getCol() + 1; i > -1 && j < 8; i--, j++) {
				if (chessboard[i][j] != null && chessboard[i][j] == king) {
					if (threat.getRow() != i+1 && threat.getCol() != j-1) {
						tempRow = i+1;
						tempCol = j-1;
					}
					break;
				}
			}
			if (tempRow != -1 && tempCol != -1) {
				for (int i = tempRow, j = tempCol; i < threat.getRow() && j > threat.getCol(); i++, j--) {
					for (Chesspiece[] tempArr : chessboard) {
						for (Chesspiece temp: tempArr) {
							if (temp != null) {
								if (temp.side == king.side) {
									if (temp.moveSet[i][j] == 1 || (temp.piece == chesstype.pawn && temp.moveSet[i][j] == 2) || (temp.piece == chesstype.pawn && temp.moveSet[i][j] == 3))
										return true;
								}
							}
						}
					}
				}
				tempRow = -1;
				tempCol = -1;
			}
			
			/*
			 * Down and left cases
			 */
			for (int i = threat.getRow() + 1, j = threat.getCol() - 1; i < 8 && j > -1; i++, j--) {
				if (chessboard[i][j] != null && chessboard[i][j] == king) {
					if (threat.getRow() != i-1 && threat.getCol() != j+1) {
						tempRow = i-1;
						tempCol = j+1;
					}
					break;
				}
			}
			if (tempRow != -1 && tempCol != -1) {
				for (int i = tempRow, j = tempCol; i > threat.getRow() && j < threat.getCol(); i--, j++) {
					for (Chesspiece[] tempArr : chessboard) {
						for (Chesspiece temp: tempArr) {
							if (temp != null) {
								if (temp.side == king.side) {
									if (temp.moveSet[i][j] == 1 || (temp.piece == chesstype.pawn && temp.moveSet[i][j] == 2) || (temp.piece == chesstype.pawn && temp.moveSet[i][j] == 3))
										return true;
								}
							}
						}
					}
				}
				tempRow = -1;
				tempCol = -1;
			}
			
			/*
			 * Down and right cases
			 */
			for (int i = threat.getRow() + 1, j = threat.getCol() + 1; i < 8 && j < 8; i++, j++) {
				if (chessboard[i][j] != null && chessboard[i][j] == king) {
					if (threat.getRow() != i-1 && threat.getCol() != j-1) {
						tempRow = i-1;
						tempCol = j-1;
					}
					break;
				}
			}
			if (tempRow != -1 && tempCol != -1) {
				for (int i = tempRow, j = tempCol; i > threat.getRow() && j > threat.getCol(); i--, j--) {
					for (Chesspiece[] tempArr : chessboard) {
						for (Chesspiece temp: tempArr) {
							if (temp != null) {
								if (temp.side == king.side) {
									if (temp.moveSet[i][j] == 1 || (temp.piece == chesstype.pawn && temp.moveSet[i][j] == 2) || (temp.piece == chesstype.pawn && temp.moveSet[i][j] == 3))
										return true;
								}
							}
						}
					}
				}
				tempRow = -1;
				tempCol = -1;
			}
		}

		return false;
	}
	
	/**
	 * Determines if the King can make a valid move.
	 * @param colNew This is the column that the King is moving to
	 * @param rowNew This is the row that the King is moving to
	 * @return boolean This determines if the King's move is valid
	 */
	public boolean isValidMove(int colNew, int rowNew) {
		int temp = 0;
		try {
			temp = moveSet[rowNew][colNew];
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		if (scanForKill(this.side, colNew, rowNew))
			return false;
		
		return (temp == 1 || temp == 2);
	}

	/**
	 * This allows the king to look into whether or not it could kill a 
	 * piece that has moved near it. This does not take into account castling or
	 * check safety. 
	 * 
	 * @param None
	 * @return None 
	 */
	public void tempUpdateMoves() {
		for (int i = 0; i < 7; i ++) {
			for (int j = 0; j < 7; j++) {
				if (moveSet[i][j] == 1) {
					moveSet[i][j] = 0;
				}
			}
		}
		
		/*
		 * Up and left
		 */
		if (row > 0 && col > 0) {
			if (chessboard[row-1][col-1] == null) {
					moveSet[row-1][col-1] = 1;
			}
			else {
				if (chessboard[row-1][col-1].side != this.side)
					moveSet[row-1][col-1] = 1;
			}
		}
		
		/*
		 * Up and right
		 */
		if (row > 0 && col < 7) {
			if (chessboard[row-1][col+1] == null) {
					moveSet[row-1][col+1] = 1;
			}
			else {
				if (chessboard[row-1][col+1].side != this.side)
					moveSet[row-1][col+1] = 1;
			}
		}
		
		/*
		 * Up
		 */
		if (row > 0) {
			if (chessboard[row-1][col] == null) {
					moveSet[row-1][col] = 1;
			}
			else {
				if (chessboard[row-1][col].side != this.side)
					moveSet[row-1][col] = 1;
			}
		}

		/*
		 * Down and left
		 */
		if (row < 7 && col > 0) {
			if (chessboard[row+1][col-1] == null) {
					moveSet[row+1][col-1] = 1;
			}
			else {
				if (chessboard[row+1][col-1].side != this.side)
					moveSet[row+1][col-1] = 1;
			}
		}
		
		/*
		 * Down and right
		 */
		if (row < 7 && col < 7) {
			if (chessboard[row+1][col+1] == null) {
					moveSet[row+1][col+1] = 1;
			}
			else {
				if (chessboard[row+1][col+1].side != this.side)
					moveSet[row+1][col+1] = 1;
			}
		}
		
		/*
		 * Down
		 */
		if (row < 7) {
			if (chessboard[row+1][col] == null) {
					moveSet[row+1][col] = 1;
			}
			else {
				if (chessboard[row+1][col].side != this.side)
					moveSet[row+1][col] = 1;
			}
		}
		
		/*
		 * Left
		 */
		if (col > 0) {
			if (chessboard[row][col-1] == null) {
					moveSet[row][col-1] = 1;
			}
			else {
				if (chessboard[row][col-1].side != this.side)
					moveSet[row][col-1] = 1;
			}
		}
		
		/*
		 * Right
		 */
		if (col < 7) {
			if (chessboard[row][col+1] == null) {
					moveSet[row][col+1] = 1;
			}
			else {
				if (chessboard[row][col+1].side != this.side)
					moveSet[row][col+1] = 1;
			}
		}
	}
}