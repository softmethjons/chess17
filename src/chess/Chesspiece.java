/**
 * 
 */
package chess;

/**
 * A chess piece. Every class that extends this will
 * inherit its fields and methods when instantiated.
 * 
 * For every chess piece's moveset but pawn:
 * 0 - Cannot move.
 * 1 - Can move/kill.
 * 
 * For pawn's moveset only:
 * 0 - Cannot move.
 * 1 - Can move/kill.
 * 2 - Can move forward
 * 3 - Can move and perform en passant
 * 
 * For king's moveset only:
 * 2 - Can castle.
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 */
public abstract class Chesspiece {

	char side;
	chesstype piece;
	int row;
	int col;
	int firstMove = 0;
	Chesspiece[][] chessboard;
	
	int[][] moveSet = new int[8][8];
	
	public Chesspiece(char side, int col, int row, chesstype chesspiece, Chesspiece[][] chessboard) {
		side = Character.toLowerCase(side);
		if (side != 'w' && side != 'b') {
			throw new IllegalArgumentException("Not a valid side!");
		}
		this.side = side;
		this.col = col;
		this.row = row;
		this.piece = chesspiece;
		this.chessboard = chessboard;		
	}
	
	/**
	 * Determines if the chess piece can make a valid move.
	 * @param colNew This is the column that the piece is moving to
	 * @param rowNew This is the row that the piece is moving to
	 * @return boolean This determines if the piece's move is valid
	 */
	public boolean isValidMove(int colNew, int rowNew) {
		int temp = 0;
		try {
			temp = moveSet[rowNew][colNew];
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		return (temp == 1);
	}
	
	/**
	 * Resets the chess piece's entire moveset to 0.
	 * @param None
	 * @return None
	 */
	public void resetBoard() {
		for (int i = 0; i < 8; i++) 
			for (int j = 0; j < 8; j++) 
				moveSet[i][j] = 0;
	}
	
	/**
	 * Sets the chess piece's position on the board.
	 * @param col The column that the piece is located in
	 * @param row The row that the piece is located in
	 * @return None
	 */
	public void setPos(int col, int row) {
		this.col = col;
		this.row = row;
	}
	
	/**
	 * Sets the chess piece's first move to be 1.
	 * @param None
	 * @return None
	 */
	public void setFirstMove() {
		firstMove = 1;
	}
	
	/**
	 * Gets the column of the chess piece.
	 * @param None
	 * @return None
	 */
	public int getCol() {
		return col;
	}
	
	/**
	 * Gets the row of the chess piece.
	 * @param None
	 * @return None
	 */
	public int getRow() {
		return row;
	}
	
	/**
	 * Determines if the chess piece that a player 
	 * is trying to move is theirs.
	 * @param x This is the player that is trying to move the piece
	 * @return boolean This determines if the piece belongs to the player
	 */
	public boolean isMyPiece(Player x) {
		return x.side == this.side;
	}
	
	/**
	 * Gets the side of the chess piece.
	 * @param None
	 * @return None
	 */
	public char getSide() {
		return side;
	}
	
	/**
	 * This will scan a given location on the array and see if any opposing piece can kill 
	 * if a piece were to be on that spot. 
	 * 
	 * @param side The side the piece I am checking is on. 
	 * @param col The column location I want to check 
	 * @param row The row location I want to check
	 * @return boolean This will indicate if an enemy piece can kill me in that location. 
	 */
	public boolean scanForKill(char side, int col, int row) {
		/*
		 * Pawns are a special case
		 */
		if (side == 'w') {
			if (col > 0 && row > 0) {
				if (chessboard[row-1][col-1] != null) {
					if (chessboard[row-1][col-1].piece == chesstype.pawn && chessboard[row-1][col-1].side != 'w') {
						return true;
					}
				}
			}
			if (col < 7 && row > 0) {
				if (chessboard[row-1][col+1] != null) {
					if (chessboard[row-1][col+1].piece == chesstype.pawn && chessboard[row-1][col+1].side != 'w') {
						return true;
					}
				}
			}
		}
		
		if (side == 'b') {
			if (col > 0 && row < 7) {
				if (chessboard[row+1][col-1] != null) {
					if (chessboard[row+1][col-1].piece == chesstype.pawn && chessboard[row+1][col-1].side != 'b') {
						return true;
					}
				}
			}
			if (col < 7 && row < 7) {
				if (chessboard[row+1][col+1] != null) {
					if (chessboard[row+1][col+1].piece == chesstype.pawn && chessboard[row+1][col+1].side != 'b') {
						return true;
					}
				}
			}
		}
		
		for (Chesspiece[] tempArr : chessboard) {
			for (Chesspiece temp: tempArr) {
				if (temp != null) {
					if (temp.side != side) {
						if (temp.moveSet[row][col] == 1)
							return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Returns the side of the chess piece along with its type.
	 * @param None
	 * @return String This is the side and type of the piece.
	 */
	public String toString() {
		return side+piece.toString();
	}
	
	/**
	 * Updates the moveset of a piece. Every piece that is created
	 * inherits this.
	 * @param None
	 * @return None
	 */
	public abstract void updateMoves();
}
