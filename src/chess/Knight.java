/**
 * 
 */
package chess;

/**
 * Knight extends Chesspiece. It is only able to move
 * in an L-shape.
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 */
public class Knight extends Chesspiece {

	public Knight(char side, int col, int row, chesstype chesspiece,
			Chesspiece[][] chessboard) {
		super(side, col, row, chesspiece, chessboard);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Updates the knight's moveset. It scans for enemies and 
	 * empty spaces. Kings can move in an L-shape, if it is
	 * valid and kill (1).
	 * @param None
	 * @return None  
	 */
	@Override
	public void updateMoves() {
		resetBoard();

		/*
		 * Up twice and left
		 */
		if (row > 1 && col > 0) {
			if (chessboard[row-2][col-1] == null)
				moveSet[row-2][col-1] = 1;
			else {
				if (chessboard[row-2][col-1].side != this.side)
					moveSet[row-2][col-1] = 1;
			}
		}

		/*
		 * Up twice and right
		 */
		if (row > 1 && col < 7) {
			if (chessboard[row-2][col+1] == null)
				moveSet[row-2][col+1] = 1;
			else {
				if (chessboard[row-2][col+1].side != this.side)
					moveSet[row-2][col+1] = 1;
			}
		}

		/*
		 * Down twice and left
		 */
		if (row < 6 && col > 0) {
			if (chessboard[row+2][col-1] == null)
				moveSet[row+2][col-1] = 1;
			else {
				if (chessboard[row+2][col-1].side != this.side)
					moveSet[row+2][col-1] = 1;
			}
		}

		/*
		 * Down twice and right
		 */
		if (row < 6 && col < 7) {
			if (chessboard[row+2][col+1] == null)
				moveSet[row+2][col+1] = 1;
			else {
				if (chessboard[row+2][col+1].side != this.side)
					moveSet[row+2][col+1] = 1;
			}
		}

		/*
		 * Left twice and up
		 */
		if (row > 0 && col > 1) {
			if (chessboard[row-1][col-2] == null)
				moveSet[row-1][col-2] = 1;
			else {
				if (chessboard[row-1][col-2].side != this.side)
					moveSet[row-1][col-2] = 1;
			}
		}
		
		/*
		 * Left twice and down
		 */
		if (row < 7 && col > 1) {
			if (chessboard[row+1][col-2] == null)
				moveSet[row+1][col-2] = 1;
			else {
				if (chessboard[row+1][col-2].side != this.side)
					moveSet[row+1][col-2] = 1;
			}
		}
		
		/*
		 * Right twice and up
		 */
		if (row > 0 && col < 6) {
			if (chessboard[row-1][col+2] == null)
				moveSet[row-1][col+2] = 1;
			else {
				if (chessboard[row-1][col+2].side != this.side)
					moveSet[row-1][col+2] = 1;
			}
		}
		
		/*
		 * Right twice and down
		 */
		if (row < 7 && col < 6) {
			if (chessboard[row+1][col+2] == null)
				moveSet[row+1][col+2] = 1;
			else {
				if (chessboard[row+1][col+2].side != this.side)
					moveSet[row+1][col+2] = 1;
			}
		}
				
	}

}
