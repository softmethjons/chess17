/**
 * 
 */
package chess;

/**
 * Chessboard is the class which emulates the 8x8 
 * chessboard itself. This keeps track of where all 
 * pieces are in relation to each other in an array. 
 * All issuing of moves (called by the player class)
 * will be dont through here. The three possible moves
 * are en passant, castling and standard moves for all other cases. 
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 */
public class Chessboard {
	public Chesspiece[][] chessboard = new Chesspiece[9][9];
	private String[][] chessboardStr = new String[9][9];
	
	/**
	 * initBoard which will take case of making the new board.
	 * Populate board with pieces for both player sides. 
	 * 
	 * @param None
	 * @return None
	 */
	public void initBoard() {
		this.blankBoard();
		
		//Handle Black Pieces 
		for (int i = 0; i < 8; i++) {
			chessboard[1][i] = new Pawn('b', i, 1, chesstype.pawn, chessboard);
		}
		for (int i = 0; i < 8; i++) {
			if (i == 0 || i == 7) {
				chessboard[0][i] = new Rook('b', i, 0, chesstype.rook, chessboard);
			}
			else if (i == 1 || i == 6) {
				chessboard[0][i] = new Knight('b', i, 0, chesstype.knight, chessboard);
			}
			else if (i == 2 || i == 5) {
				chessboard[0][i] = new Bishop('b', i, 0, chesstype.bishop, chessboard);
			}
			else if (i == 3) {
				chessboard[0][i] = new Queen('b', i, 0, chesstype.queen, chessboard);
			}
			else {
				chessboard[0][i] = new King('b', i, 0, chesstype.king, chessboard, this);
			}
		}
		
		//Handle White Pieces
		for (int i = 0; i < 8; i++) {
			chessboard[6][i] = new Pawn('w', i, 6, chesstype.pawn, chessboard);
		}
		for (int i = 0; i < 8; i++) {
			if (i == 0 || i == 7) { 
				chessboard[7][0] = new Rook('w', 0, 7, chesstype.rook, chessboard);
				chessboard[7][7] = new Rook('w', 7, 7, chesstype.rook, chessboard);
				
			}
			else if (i == 1 || i == 6) {
				chessboard[7][i] = new Knight('w', i, 7, chesstype.knight, chessboard);
			}
			else if (i == 2 || i == 5) {
				chessboard[7][i] = new Bishop('w', i, 7, chesstype.bishop, chessboard);
			}
			else if (i == 3) {
				chessboard[7][i] = new Queen('w', i, 7, chesstype.queen, chessboard);
			}
			else {
				chessboard[7][i] = new King('w', i, 7, chesstype.king, chessboard, this);
			}
		}
		
		updateMoves();
	}
	
	/**
	 * This will issue a move to the board. It will perform
	 * checks to see if the side issuing the move is not
	 * putting their king in danger. If it detects danger, 
	 * the move will be undone. Promoting a pawn will occur through 
	 * here.
	 * 
	 *  @param colCurr This is the current column position of the piece to move.
	 *  @param rowCurr This is the current row position of the piece to move. 
	 *  @param colNew This is the new column position the piece will move to. 
	 *  @param rowNew This is the new row position the piece will move to. 
	 *  @param mess This is the string to read if the player wants to promote pawn. 
	 *  @return boolean This returns if the move was done or not.  
	 */
	public boolean move(int colCurr, int rowCurr, int colNew, int rowNew, String mess) {
		Chesspiece takenPiece = chessboard[rowNew][colNew];
		
		chessboard[rowNew][colNew] = chessboard[rowCurr][colCurr];
		chessboard[rowNew][colNew].setPos(colNew, rowNew);
		chessboard[rowNew][colNew].firstMove++;
		chessboard[rowCurr][colCurr] = null;
		
		if (isKingInCheck(colNew, rowNew, colCurr, rowCurr, takenPiece)) 
			return false;
		
		if (chessboard[rowNew][colNew].piece == chesstype.pawn) {
			Pawn temp = (Pawn) chessboard[rowNew][colNew]; 
			temp.enpassant += Math.abs(rowCurr - rowNew);
		}
		
		if (chessboard[rowNew][colNew].piece == chesstype.pawn) {
			if ((chessboard[rowNew][colNew].side == 'w' && rowNew == 0) || (chessboard[rowNew][colNew].side == 'b' && rowNew == 7))
				promote(colNew, rowNew, mess);
		}
		
		increaseEpCount();
		updateMoves();	
		return true;
	}
	
	/**
	 * Issues a castling move. Castling safety is handled in king. 
	 * If castle is called, the move is ensured safe already. 
	 * 
	 * @param colCurr This is the current column position of the king.
	 * @param rowCurr This is the current row position of the king. 
	 * @param colNew This is the new column position of the king. 
	 * @param rowNew This is the new row position of the king.
	 * @return none 
	 */
	public void castle(int colCurr, int rowCurr, int colNew, int rowNew) {
		chessboard[rowNew][colNew] = chessboard[rowCurr][colCurr];
		chessboard[rowNew][colNew].setPos(colNew, rowNew);
		chessboard[rowNew][colNew].setFirstMove();
		chessboard[rowCurr][colCurr] = null;
		if (colNew == 2) {
			chessboard[rowNew][colNew+1] = chessboard[rowNew][0];
			chessboard[rowNew][colNew+1].setPos(colNew+1, rowNew);
			chessboard[rowNew][colNew+1].setFirstMove();
			chessboard[rowNew][0] = null;
		}
		if (colNew == 6) {
			chessboard[rowNew][colNew-1] = chessboard[rowNew][7];
			chessboard[rowNew][colNew-1].setPos(colNew-1, rowNew);
			chessboard[rowNew][colNew-1].setFirstMove();
			chessboard[rowNew][7] = null;
		}	
		
		increaseEpCount();
		updateMoves();
	}
	
	/**
	 * This will issue an en passant move to the board. The Pawn class
	 * takes care of whether or not there is an opposing
	 * pawn to perform the move on. However, there will be
	 * checks to see if the side issuing the move is not
	 * putting their king in danger. If it detects danger, 
	 * the move will be undone. Promoting a pawn will occur through 
	 * here.
	 * 
	 *  @param colCurr This is the current column position of the piece to move.
	 *  @param rowCurr This is the current row position of the piece to move. 
	 *  @param colNew This is the new column position the piece will move to. 
	 *  @param rowNew This is the new row position the piece will move to. 
	 *  @return boolean This returns if the move was done or not.  
	 */
	public boolean enPassant(int colCurr, int rowCurr, int colNew, int rowNew) {
		Chesspiece taken = chessboard[rowCurr][colNew];
		
		chessboard[rowNew][colNew] = chessboard[rowCurr][colCurr];
		chessboard[rowCurr][colCurr] = null;
		chessboard[rowCurr][colNew] = null;
		
		if (isKingInCheck(colNew, rowNew, colCurr, rowCurr, taken)) {
			chessboard[rowNew][colNew] = null;
			chessboard[rowCurr][colNew] = taken;
			chessboard[rowCurr][colNew].setPos(colNew, rowCurr);
			chessboard[rowCurr][colNew].firstMove++;
			updateMoves();
			return false;
		}
		
		chessboard[rowNew][colNew].setPos(colNew, rowCurr);
		increaseEpCount();
		updateMoves();
		return true;
	}
	
	/**
	 * Increase the epCount of all pawns that have made a move on the
	 * board. This is to allow the Pawns to know whether or not the 
	 * time to perform en passant has passed or not. 
	 * 
	 *  @param None
	 *  @return None
	 */
	private void increaseEpCount() {
		for (Chesspiece[]temp: chessboard) {
			for (Chesspiece piece: temp) {
				if (piece != null) {
					if (piece.piece == chesstype.pawn) {
						Pawn pawn = (Pawn) piece;
						if (pawn.enpassant != 0)
							pawn.epCount++;
					}
				}
			}
		}
	}
	
	/**
	 * This will perform a safety check to see if the
	 * move made by the player is legal. If the king of that player
	 * is in danger, the move will be undone. 
	 * 
	 * @param colCurr This is the old column position of the moved piece.
	 * @param rowCurr This is the old row position of the moved piece. 
	 * @param colNew This is the current column position of the piece. 
	 * @param rowNew This is the current row position of the piece. 
	 * @param takePiece The opposing piece that was taken by the player. 
	 * @return boolean This returns if the move is legal or not. 
	 */
	public boolean isKingInCheck(int colNew, int rowNew, int colCurr, int rowCurr, Chesspiece takenPiece) {
		updateMoves();
		
		if (chessboard[rowNew][colNew].piece == chesstype.king) {
			King temp = (King)chessboard[rowNew][colNew];
			if (temp.isCheck()) {
				chessboard[rowCurr][colCurr] = chessboard[rowNew][colNew];
				chessboard[rowNew][colNew] = takenPiece;
				chessboard[rowCurr][colCurr].firstMove--;
				chessboard[rowCurr][colCurr].setPos(colCurr, rowCurr);	
				updateMoves();
				return true;
			}
		}
		
		/*
		 * This is to check if moving your own piece has resulted in 
		 * the king being left in check this is a no no and we will 
		 * undo again.  
		 */
		if (chessboard[rowNew][colNew].piece != chesstype.king) {
			King k = null;
			
			for (Chesspiece[] temp: chessboard) {
				for (Chesspiece piece: temp) {
					if (piece != null && piece.piece == chesstype.king && piece.side == chessboard[rowNew][colNew].side)
						k = (King)piece;
				}
			}
			
			if (k != null && k.isCheck()) {
				chessboard[rowCurr][colCurr] = chessboard[rowNew][colNew];
				chessboard[rowNew][colNew] = takenPiece;
				chessboard[rowCurr][colCurr].firstMove--;
				chessboard[rowCurr][colCurr].setPos(colCurr, rowCurr);
				updateMoves();
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks for checkmate, calls the player's 
	 * king to see if it is checked first. If it 
	 * is call the King's checkmate function which will
	 * check to see if it has any possible moves left, 
	 * if any friendly piece can block the check 
	 * or if the opposing piece can be killed. 
	 * 
	 * @param side The play you want to check for checkmate. 
	 * @return boolean This indicates if the player is checkmated or not. 
	 */
	public boolean isCheckmate(char side) {
		if (!isCheck(side)) {
			return false;
		}
		
		for (Chesspiece[] temp: chessboard) {
			for (Chesspiece piece: temp) {
				if (piece != null && piece.piece == chesstype.king && piece.side == side) {
					King king = (King) piece;
					return king.isCheckmate();
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Looks for the king to call isCheck() function. 
	 * 
	 * @param side This will indicate the King that is needed to be checked.
	 * @return boolean This will indicate if the king is in check or not. 
	 */
	public boolean isCheck(char side) {
		for (Chesspiece[] temp: chessboard) {
			for (Chesspiece piece: temp) {
				if (piece != null && piece.piece == chesstype.king && piece.side == side) {
					King king = (King) piece;
					return king.isCheck();
				}
			}
		}
		return false;
	}
	
	/**
	 * Check for a stalemate. A stalemate here is considered when a player
	 * has no valid move on any piece left. 
	 * 
	 * @param side This is the side we want to check for stalemate. 
	 * @return boolean This indicates if that player is in a stalemate. 
	 */
	public boolean isStalemate(char side) {
		for (Chesspiece[] temp: chessboard) {
			for (Chesspiece piece: temp) {
				if (piece != null)
					if (piece.piece == chesstype.king && piece.side == side) {
						King king = (King) piece;
						if (king.isCheck()) {
							return false;
						}
					}
			}
		}
		
		for (Chesspiece[] temp: chessboard) {
			for (Chesspiece piece: temp) {
				if (piece != null) {
					if (piece.side == side) {
						for (int i = 0; i < 8; i++) {
							for (int j = 0; j < 8; j++) {
								if (piece.moveSet[i][j] == 1)
									return false;
							}
						}
					}
				}
			}
		}
		return true;
	}
	
	/**
	 * Needed for correct king moveset implementation. 
	 * This checks to see if the king would be in check after taking any piece. 
	 * If the king would be in check, this will return true, indicating that the king
	 * should not move there. 
	 * 
	 * @param side The side's king whom we want to check. 
	 * @param colNew This is the new column position where the king may move.
	 * @param rowNew This is the new row position where the king may move. 
	 * @param colCurr This is the current column position of the king. 
	 * @param rowCurr This is the current row position of the king. 
	 * @return boolean This will indicate if the spot the king can move to would result in a check. 
	 */
	public boolean falsePositive(char side, int colNew, int rowNew, int colCurr, int rowCurr) {
		
		boolean isFalse = false;
		
		Chesspiece taken = chessboard[rowNew][colNew];
		
		if (taken == null)
			return false;
		
		chessboard[rowNew][colNew] = chessboard[rowCurr][colCurr];
		chessboard[rowCurr][colCurr] = null;
		
		for (Chesspiece[] temp: chessboard) {
			for(Chesspiece piece: temp) {
				if (piece != null) {
					if (piece.side != side) {
						if (piece.piece != chesstype.king) {
							piece.updateMoves();
							if (piece.moveSet[rowNew][colNew] == 1)
								isFalse = true;
						}
						else {
							King king = (King) piece;
							king.tempUpdateMoves();
							if (king.moveSet[rowNew][colNew] == 1)
								isFalse = true;
						}
					}
				}
			}
		}
		
		chessboard[rowCurr][colCurr] = chessboard[rowNew][colNew];
		chessboard[rowNew][colNew] = taken;
		
		for (Chesspiece[] temp: chessboard) {
			for(Chesspiece piece: temp) {
				if (piece != null) {
					if (piece.side != side) {
						if (piece.piece != chesstype.king)
							piece.updateMoves();
						else {
							King king = (King) piece;
							king.tempUpdateMoves();
						}
					}
				}
			}
		}
		
		
		return isFalse;
	}
	
	/**
	 * Given the way scanForKill works, 
	 * it is important we run the scan twice. 
	 * This will allow the king to have the correct moveset
	 * when everything is done. updateMoves() will go through 
	 * the entire chessboard and call each chesspiece's 
	 * updateMove() function. 
	 * 
	 * @param None
	 * @return None
	 */
	private void updateMoves() {
		for (Chesspiece[] temp: chessboard) {
			for (Chesspiece piece: temp) {
				if (piece != null)
					piece.updateMoves();
			}
		}
		for (Chesspiece[] temp: chessboard) {
			for (Chesspiece piece: temp) {
				if (piece != null)
					piece.updateMoves();
			}
		}
	}

	/**
	 * Promotes a pawn to a new chesspiece. Queen is the default.  
	 * 
	 * @param colNew New column position of the promoted pawn.
	 * @param rowNew New row position of the promoted pawn. 
	 * @param mess The string the player has entered to indicate new promoted piece type. 
	 * @return None
	 */
	public void promote(int colNew, int rowNew, String mess) {
		if (chessboard[rowNew][colNew].side == 'w' && rowNew == 0) {
			if (mess.equalsIgnoreCase("B")) {
				chessboard[rowNew][colNew] = null;
				chessboard[rowNew][colNew] = new Bishop('w', colNew, rowNew, chesstype.bishop, chessboard);
				chessboard[rowNew][colNew].setPos(colNew, rowNew);
				chessboard[rowNew][colNew].setFirstMove();
			}
			else if (mess.equalsIgnoreCase("N")) {
				chessboard[rowNew][colNew] = null;
				chessboard[rowNew][colNew] = new Knight('w', colNew, rowNew, chesstype.knight, chessboard);
				chessboard[rowNew][colNew].setPos(colNew, rowNew);
				chessboard[rowNew][colNew].setFirstMove();
			}
			else if (mess.equalsIgnoreCase("R")) {
				chessboard[rowNew][colNew] = null;
				chessboard[rowNew][colNew] = new Rook('w', colNew, rowNew, chesstype.rook, chessboard);
				chessboard[rowNew][colNew].setPos(colNew, rowNew);
				chessboard[rowNew][colNew].setFirstMove();
			}
			else if (mess.equalsIgnoreCase("Q")) {
				chessboard[rowNew][colNew] = null;
				chessboard[rowNew][colNew] = new Queen('w', colNew, rowNew, chesstype.queen, chessboard);
				chessboard[rowNew][colNew].setPos(colNew, rowNew);
				chessboard[rowNew][colNew].setFirstMove();
			}
			else if (mess.equals("")) {
				chessboard[rowNew][colNew] = null;
				chessboard[rowNew][colNew] = new Queen('w', colNew, rowNew, chesstype.queen, chessboard);
				chessboard[rowNew][colNew].setPos(colNew, rowNew);
				chessboard[rowNew][colNew].setFirstMove();
			}
		}
		else if (chessboard[rowNew][colNew].side == 'b' && rowNew == 7) {
			if (mess.equalsIgnoreCase("B")) {
				chessboard[rowNew][colNew] = null;
				chessboard[rowNew][colNew] = new Bishop('b', colNew, rowNew, chesstype.bishop, chessboard);
				chessboard[rowNew][colNew].setPos(colNew, rowNew);
				chessboard[rowNew][colNew].setFirstMove();
			}
			else if (mess.equalsIgnoreCase("N")) {
				chessboard[rowNew][colNew] = null;
				chessboard[rowNew][colNew] = new Knight('b', colNew, rowNew, chesstype.knight, chessboard);
				chessboard[rowNew][colNew].setPos(colNew, rowNew);
				chessboard[rowNew][colNew].setFirstMove();
			}
			else if (mess.equalsIgnoreCase("R")) {
				chessboard[rowNew][colNew] = null;
				chessboard[rowNew][colNew] = new Rook('b', colNew, rowNew, chesstype.rook, chessboard);
				chessboard[rowNew][colNew].setPos(colNew, rowNew);
				chessboard[rowNew][colNew].setFirstMove();
			}
			else if (mess.equalsIgnoreCase("Q")) {
				chessboard[rowNew][colNew] = null;
				chessboard[rowNew][colNew] = new Queen('b', colNew, rowNew, chesstype.queen, chessboard);
				chessboard[rowNew][colNew].setPos(colNew, rowNew);
				chessboard[rowNew][colNew].setFirstMove();
			}
			else if (mess.equals("")) {
				chessboard[rowNew][colNew] = null;
				chessboard[rowNew][colNew] = new Queen('b', colNew, rowNew, chesstype.queen, chessboard);
				chessboard[rowNew][colNew].setPos(colNew, rowNew);
				chessboard[rowNew][colNew].setFirstMove();
			}
		}
	}
	
	/** 
	 * Resets the string array board. The string 
	 * array board is the visual representation of 
	 * pieces for the player to see in terminal. This
	 * clears the board and reprints all checkboxes. 
	 * 
	 * @param None
	 * @return None
	 */
	private void blankBoard() {
		for (int i = 0; i < 7; i += 2) {
			for (int j = 0; j < 8; j++) {
				if (j % 2 == 0) {
					chessboardStr[i][j] = "  ";
				} else {
					chessboardStr[i][j] = "##";
				}
			}
		}
		for (int i = 1; i < 8; i += 2) {
			for (int j = 0; j < 8; j++) {
				if (j % 2 == 0) {
					chessboardStr[i][j] = "##";
				} else {
					chessboardStr[i][j] = "  ";
				}
			}
		}
		for (int i = 0; i < 8; i++) {
			chessboardStr[i][8] = String.valueOf(8 - i);
		}
		for (int i = 0; i < 8; i++) {
			char c = (char) ('a' + i);
			chessboardStr[8][i] = String.valueOf(" "+c);
		}
		chessboardStr[8][8] = " ";
	}
	
	/**
	 * This will scan the Chesspiece array and print
	 * to the string array the string representation of 
	 * chesspiece objects in their respective location 
	 * on the string array. 
	 * 
	 * @param None
	 * @return None
	 */
	public void printBoard() {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (chessboard[i][j] != null) 
					System.out.print(chessboard[i][j] + " ");
				else
					System.out.print(chessboardStr[i][j] + " ");
			}
			System.out.println();
		}
	}
}