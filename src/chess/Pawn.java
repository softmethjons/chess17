/**
 * 
 */
package chess;

/**
 * Pawn extends Chesspiece. It uses several special cases
 * in its moveset to deal with en passant, moving forward and
 * killing enemy pieces. 
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 */
public class Pawn extends Chesspiece{

	public Pawn(char side, int col, int row, chesstype chesspiece, Chesspiece[][] chessboard) {
		super(side, col, row, chesspiece, chessboard);
	}

	int enpassant = 0;
	int epCount = 0;
	
	/**
	 * Updates the pawn's moveset. Scan for enemies and 
	 * empty spaces. Pawn can move forward but not 
	 * kill (2). Pawn can move and kill (1). Pawn can perform 
	 * en passant (3).
	 * @param None
	 * @return None  
	 */
	@Override
	public void updateMoves() {
		resetBoard();

		if (side == 'w') {
			if (firstMove != 0 && row != 0) {
				if (chessboard[row-1][col] == null)
					moveSet[row-1][col] = 2;
				if (col != 0) {
					if (chessboard[row-1][col-1] != null) {
						if (chessboard[row-1][col-1].side != 'w') 
							moveSet[row-1][col-1] = 1;
					}
				}
				if (col != 7) {
					if (chessboard[row-1][col+1] != null) {
						if (chessboard[row-1][col+1].side != 'w') 
							moveSet[row-1][col+1] = 1;
					}
				}
				if (row == 3) {
					if (col > 0) {
						Chesspiece temp = chessboard[row][col-1];
						if (temp != null) {
							if (temp.piece == chesstype.pawn && temp.side != side) {
								Pawn pawn = (Pawn) temp; 
								if (pawn.enpassant == 2 && pawn.epCount == 1)
									moveSet[2][col-1] = 3;
							}
						}
					}
					if (col < 7) {
						Chesspiece temp = chessboard[row][col+1];
						if (temp != null) {
							if (temp.piece == chesstype.pawn && temp.side != side) {
								Pawn pawn = (Pawn) temp; 
								if (pawn.enpassant == 2 && pawn.epCount == 1)
									moveSet[2][col+1] = 3;
							}
						}
					}
				}
			}
			
			else if (firstMove == 0){
				if (chessboard[row-1][col] == null)
					moveSet[row-1][col] = 2;
				if (chessboard[row-2][col] == null)
					moveSet[row-2][col] = 2;
				if (col != 0) {
					if (chessboard[row-1][col-1] != null) {
						if (chessboard[row-1][col-1].side != 'w') 
							moveSet[row-1][col-1] = 1;
					}
				}	
				if (col != 7) {
					if (chessboard[row-1][col+1] != null) {
						if (chessboard[row-1][col+1].side != 'w') 
							moveSet[row-1][col+1] = 1;
					}
				}
			}
		}
		
		else {
			if (firstMove != 0 && row != 7) {
				if (chessboard[row+1][col] == null)
					moveSet[row+1][col] = 2;
				if (col != 0) {
					if (chessboard[row+1][col-1] != null) {
						if (chessboard[row+1][col-1].side != 'b') 
							moveSet[row+1][col-1] = 1;
					}
				}
				if (col != 7) {
					if (chessboard[row+1][col+1] != null) {
						if (chessboard[row+1][col+1].side != 'b') 
							moveSet[row+1][col+1] = 1;
						
					}
				}
				if (row == 4) {
					if (col > 0) {
						Chesspiece temp = chessboard[row][col-1];
						if (temp != null) {
							if (temp.piece == chesstype.pawn && temp.side != side) {
								Pawn pawn = (Pawn) temp; 
								if (pawn.enpassant == 2 && pawn.epCount == 1)
									moveSet[5][col-1] = 3;
							}
						}
					}
					if (col < 7) {
						Chesspiece temp = chessboard[row][col+1];
						if (temp != null) {
							if (temp.piece == chesstype.pawn && temp.side != side) {
								Pawn pawn = (Pawn) temp; 
								if (pawn.enpassant == 2 && pawn.epCount == 1)
									moveSet[5][col+1] = 3;
							}
						}
					}
				}
			}
			else if (firstMove == 0){
				if (chessboard[row+1][col] == null)
					moveSet[row+1][col] = 2;
				if (chessboard[row+2][col] == null)
					moveSet[row+2][col] = 2;
				if (col != 0) {
					if (chessboard[row+1][col-1] != null) {
						if (chessboard[row+1][col-1].side != 'b') 
							moveSet[row+1][col-1] = 1;
					}
				}
				if (col != 7) {
					if (chessboard[row+1][col+1] != null) {
						if (chessboard[row+1][col+1].side != 'b') 
							moveSet[row+1][col+1] = 1;
						
					}
				}
			}
		}
	}


   /**
	* Checks to see if pawn can make a move forward or 
	* perform en passant. This is different from other pieces
	* that can move and kill. 
	* 
	* @param None
	* @return None
	*/
	public boolean isValidMove(int colNew, int rowNew) {
		int temp = 0;
		try {
			temp = moveSet[rowNew][colNew];
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		return (temp == 2 || temp == 3);
	}

	/**
 	* This is for checking if the square the pawn 
 	* will move to has a piece to kill. If it does not, 
 	* the pawn should not be able to move there. 
 	* 
 	* @param None
 	* @return None
	*/
	public boolean isKillable(int colNew, int rowNew) {
		int temp = 0;
		try {
			temp = moveSet[rowNew][colNew];
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		return (temp == 1);
	}

}