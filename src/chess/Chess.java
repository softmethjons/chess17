/**
 * 
 */
package chess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * <h1>Chess Software Methodology</h1>
 * Chess program to allow two players to play a game of chess
 * through terminal. Players can perform standard chess moves, castling, 
 * en passant, resign and draw. Automatic draws were not implemented.
 * The game ends when players agree to draw, one player resigns, a checkmate
 * is reached or stalemate is reached. 
 * 
 * Chess contains the main program where board and players are
 * initialized. 
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 */
public class Chess {

	/**
	 * Main method, loads the board and players. This will prompt 
	 * the players with texts on what to do next. 
	 * This will continue the game until a winner, stalemate or draw
	 * is determined. 
	 * 
	 * @param args unused
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Chessboard cb = new Chessboard();
		Player playerB = new Player ('b',cb);
		Player playerW = new Player ('w', cb);
		
		cb.initBoard();
		cb.printBoard();
		System.out.println();
		
		String message1 = "";
		String message2 = "";
		
		StringTokenizer st1;
		StringTokenizer st2;
		
		while (true) {
			if (cb.isCheckmate('w')) {
				System.out.println("Checkmate: Black wins.");
				System.exit(0);
			}
			
			if (cb.isCheck('w')) {
				System.out.println("White King is in check");
			}
			
			if (cb.isStalemate('w')) {
				System.out.println("White King is in stalemate");
				System.exit(0);
			}
			
			System.out.print("White Player Enter Move: ");
			String play = br.readLine();
			play = play.toLowerCase();
			System.out.println();
			
			st1 = new StringTokenizer(play);
			
			if (st1.hasMoreTokens())
				message1 = st1.nextToken();
			
			if (message1.equalsIgnoreCase("draw") && message2.equalsIgnoreCase("draw?") && !st1.hasMoreTokens()) {
				System.out.println("Draw.");
				System.exit(0);
			}
			
			if (message1.equalsIgnoreCase("resign") && !st1.hasMoreTokens()) {
				System.out.println("Black wins.");
				System.exit(0);
			}
			
			while (playerW.move(play) == false) {
				System.out.println("Illegal move, try again.\n");
				cb.printBoard();
				System.out.println();
				
				if (cb.isCheck('w')) {
					System.out.println("White King is in check");
				}
				
				System.out.print("White Player Enter Move: ");
				play = br.readLine();
				play = play.toLowerCase();
				System.out.println();
				
				st1 = new StringTokenizer(play);
				
				if (st1.hasMoreTokens())
					message1 = st1.nextToken();
				
				if (message1.equalsIgnoreCase("draw") && message2.equalsIgnoreCase("draw?") && !st1.hasMoreTokens()) {
					System.out.println("Draw.");
					System.exit(0);
				}
				
				if (message1.equalsIgnoreCase("resign") && !st1.hasMoreTokens()) {
					System.out.println("Black wins.");
					System.exit(0);
				}
			}
			
			cb.printBoard();
			System.out.println();
			
			if (cb.isCheckmate('b')) {
				System.out.println("Checkmate: White wins.");
				System.exit(0);
			}
			
			if (cb.isCheck('b')) {
				System.out.println("Black King is in check");
			}
			
			if (cb.isStalemate('b')) {
				System.out.println("Black King is in stalemate");
				System.exit(0);
			}
					
			if (st1.hasMoreTokens())
				message1 = st1.nextToken();
			if (st1.hasMoreTokens())
				message1 = st1.nextToken();
			
			System.out.print("Black Player Enter Move: ");
			play = br.readLine();
			play = play.toLowerCase();
			System.out.println();
			
			st2 = new StringTokenizer(play);
			
			if (st2.hasMoreTokens())
				message2 = st2.nextToken();
			
			if (message1.equalsIgnoreCase("draw?") && message2.equalsIgnoreCase("draw") && !st2.hasMoreTokens()) {
				System.out.println("Draw.");
				System.exit(0);
			}
			
			if (message2.equalsIgnoreCase("resign") && !st2.hasMoreTokens()) {
				System.out.println("White wins.");
				System.exit(0);
			}
						
			while (playerB.move(play) == false) {
				System.out.println("Illegal move, try again.\n");
				cb.printBoard();
				System.out.println();
				
				if (cb.isCheck('b')) {
					System.out.println("Black King is in check");
				}
				
				System.out.print("Black Player Enter Moves: ");
				play = br.readLine();
				play = play.toLowerCase();
				System.out.println();
				
				st2 = new StringTokenizer(play);
				
				if (st2.hasMoreTokens())
					message2 = st2.nextToken();
				
				if (message1.equalsIgnoreCase("draw?") && message2.equalsIgnoreCase("draw") && !st2.hasMoreTokens()) {
					System.out.println("Draw.");
					System.exit(0);
				}
				
				if (message2.equalsIgnoreCase("resign") && !st2.hasMoreTokens()) {
					System.out.println("White wins.");
					System.exit(0);
				}
				
			}		
			cb.printBoard();
			System.out.println();
			
			if (st2.hasMoreTokens())
				message2 = st2.nextToken();
			if (st2.hasMoreTokens())
				message2 = st2.nextToken();
			
		}

	}

}