/**
 * 
 */
package chess;

/**
 * Queen extends Chesspiece. It is able to move in
 * any direction, and can continue moving in one of
 * those directions if it is not blocked or does not 
 * go out of bounds.
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 */
public class Queen extends Chesspiece{

	public Queen(char side, int col, int row, chesstype chesspiece,
			Chesspiece[][] chessboard) {
		super(side, col, row, chesspiece, chessboard);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Updates the Queen's moveset. It scans for enemies and 
	 * empty spaces. Queens can move in any direction, if it
	 * is valid, and kill (1).
	 * @param None
	 * @return None  
	 */
	@Override
	public void updateMoves() {
		resetBoard();
		/*
		 * Handle horizontal traversal
		 */
		for (int i = col+1; i < 8; i++) {
			if (chessboard[row][i] != null) {
				if (chessboard[row][i].side != this.side) {
					moveSet[row][i] = 1;
				}
				break;
			}
			moveSet[row][i] = 1;
		}
		for (int i = col-1; i > -1; i--) {
			if (chessboard[row][i] != null) {
				if (chessboard[row][i].side != this.side) {
					moveSet[row][i] = 1;
				}
				break;
			}
			moveSet[row][i] = 1;
		}
		
		/*
		 * Handle vertical traversal
		 */
		for (int i = row+1; i < 8; i++) {
			if (chessboard[i][col] != null) {
				if (chessboard[i][col].side != this.side) {
					moveSet[i][col] = 1;
				}
				break;
			}
			moveSet[i][col] = 1;
		}
		for (int i = row-1; i > -1; i--) {
			if (chessboard[i][col] != null) {
				if (chessboard[i][col].side != this.side) {
					moveSet[i][col] = 1;
				}
				break;
			}
			moveSet[i][col] = 1;
		}
		
		/*
		 * Up and left
		 */
		for (int i = row - 1, j = col - 1; i > -1 && j > -1; i--, j--)	{
			if (chessboard[i][j] != null) {
				if (chessboard[i][j].side != this.side) {
					moveSet[i][j] = 1;
				}
				break;
			}
			moveSet[i][j] = 1;
		}
		
		/*
		 * up and right
		 */
		for (int i = row - 1, j = col + 1; i > -1 && j < 8; i--, j++) {
			if (chessboard[i][j] != null) {
				if (chessboard[i][j].side != this.side) {
					moveSet[i][j] = 1;
				}
				break;
			}
			moveSet[i][j] = 1;
		}
		
		/*
		 * down and right
		 */
		for (int i = row + 1, j = col + 1; i < 8 && j < 8; i++, j++) {
			if (chessboard[i][j] != null) {
				if (chessboard[i][j].side != this.side) {
					moveSet[i][j] = 1;
				}
				break;
			}
			moveSet[i][j] = 1;
		}
		
		/*
		 * down and left
		 */
		for (int i = row + 1, j = col - 1; i < 8 && j > -1; i++, j--) {
			if (chessboard[i][j] != null) {
				if (chessboard[i][j].side != this.side) {
					moveSet[i][j] = 1;
				}
				break;
			}
			moveSet[i][j] = 1;
		}
	}	
}
