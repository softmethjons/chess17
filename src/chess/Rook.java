/**
 * 
 */
package chess;

/**
 * Rook extends Chesspiece. It is able to move horizontally
 * and vertically, and can continue moving in one of those
 * directions if it is not blocked or does not go out of bounds.
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 */
public class Rook extends Chesspiece{
	
	public Rook(char side, int col, int row, chesstype chesspiece, Chesspiece[][] chessboard) {
		super(side, col, row, chesspiece, chessboard);
	}
	
	/**
	 * Updates the Rook's moveset. It scans for enemies and 
	 * empty spaces. Rooks can move horizontally and vertically,
	 * if it is valid, and kill (1).
	 * @param None
	 * @return None  
	 */
	@Override
	public void updateMoves() {
		resetBoard();
		
		/*
		 * Handle horizontal traversal
		 */
		for (int i = col+1; i < 8; i++) {
			if (chessboard[row][i] != null) {
				if (chessboard[row][i].side != this.side) {
					moveSet[row][i] = 1;
				}
				break;
			}
			moveSet[row][i] = 1;
		}
		for (int i = col-1; i > -1; i--) {
			if (chessboard[row][i] != null) {
				if (chessboard[row][i].side != this.side) {
					moveSet[row][i] = 1;
				}
				break;
			}
			moveSet[row][i] = 1;
		}
		
		/*
		 * Handle vertical traversal
		 */
		for (int i = row+1; i < 8; i++) {
			if (chessboard[i][col] != null) {
				if (chessboard[i][col].side != this.side) {
					moveSet[i][col] = 1;
				}
				break;
			}
			moveSet[i][col] = 1;
		}
		for (int i = row-1; i > -1; i--) {
			if (chessboard[i][col] != null) {
				if (chessboard[i][col].side != this.side) {
					moveSet[i][col] = 1;
				}
				break;
			}
			moveSet[i][col] = 1;
		}
	}

}
