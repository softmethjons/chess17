/**
 * 
 */
package chess;

/**
 * Enum states for chesspiece types. This will allow
 * us to quickly identify what each chesspiece type is
 * and provides the letter to print for each piece type. 
 * 
 * @author Jonathan Caverly
 * @author Jonathan Getahun
 */
public enum chesstype {
	pawn('p'),
	rook('R'),
	knight('N'),
	bishop('B'),
	queen('Q'),
	king('K');
	
	private final char nameType;
	
	chesstype(char nameType) {
		this.nameType = nameType;
	}
	
	/**
	 * returns the string representation of the enum. 
	 */
	@Override
	public String toString() {
		return this.nameType + "";
	}
}
